import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order } from './entities/order.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { OrderItem } from './entities/order-item';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    @InjectRepository(Product)
    private productsRespository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemsRespository: Repository<OrderItem>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    console.log(createOrderDto);
    const customer = await this.customersRepository.findOneBy({
      id: createOrderDto.customerId,
    });
    console.log(customer);
    const order = new Order();
    order.customer = customer;
    order.amount = 0;
    order.total = 0;
    await this.ordersRepository.save(order); //order ได้รับไอดีมาแล้ว

    const orderItems: OrderItem[] = await Promise.all(
      createOrderDto.orderItems.map(async (od) => {
        const orderItem = new OrderItem();
        orderItem.amount = od.amount;
        orderItem.product = await this.productsRespository.findOneBy({
          id: od.productId,
        });
        orderItem.name = orderItem.product.name;
        orderItem.price = orderItem.product.price;
        orderItem.total = orderItem.price * orderItem.amount;
        orderItem.order = order; //การอ้างกลับ
        return orderItem;
      }),
    );
    for (const orderItem of orderItems) {
      this.orderItemsRespository.save(orderItem);
      order.amount = order.amount + orderItem.amount;
      order.total = order.total + orderItem.total;
    }
    await this.ordersRepository.save(order);
    return await this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['customer', 'orderItems'],
    });
  }

  findOne(id: number) {
    return this.ordersRepository.findOne({
      where: { id: id },
      relations: ['customer', 'orderItems'],
    });
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.ordersRepository.findOneBy({ id: id });
    return this.ordersRepository.softRemove(order);
  }
}
